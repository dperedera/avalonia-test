﻿using System;
using System.Collections.Generic;
using System.Text;
using ReactiveUI;

namespace avalonia_hello.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        public string Greeting => "Welcome to Avalonia!";
        private string _message = "";

        private string _num1 = "0";
        private string _num2 = "0";
        private string _result = "0";

        public void Click()
        {
            int num1 = 0;
            int num2 = 0;
            int result = 0;
            Message = "";

            try
            {
                num1 = Convert.ToInt32(Num1);
                num2 = Convert.ToInt32(Num2);
            }
            catch(Exception e)
            {
                Message = "Wrong input!";
                Console.WriteLine(e.Message);
            }

            result = num1 + num2;

            Result = result.ToString();
        }

        public string Message //Так свойства используются для привязки в ReactiveUI
        {
            get => _message;
            set => this.RaiseAndSetIfChanged(ref _message, value);
        }

        public string Num1 
        {
            get => _num1; 
            set => this.RaiseAndSetIfChanged(ref _num1, value);
        }


        public string Num2
        {
            get => _num2;
            set => this.RaiseAndSetIfChanged(ref _num2, value);
        }

        public string Result
        {
            get => _result;
            set => this.RaiseAndSetIfChanged(ref _result, value);
        }
    }
}